build:
	docker-compose build
run:
	docker-compose up -d
stop:
	docker compose stop

install:
	pip install -r requirements.txt

isort:
	isort .
  
start:
	uvicorn main:app --reload

test:
	pytest tests



venv:

ifeq ($(OS),Windows_NT)
	@if exist venv rmdir /s /q venv
	@python -m venv venv
	@echo "To activate the virtual environment, run: .\\venv\\Scripts\\activate"
else
	@rm -rf venv
	@python3 -m venv venv
	@echo "To activate the virtual environment, run: source venv/bin/activate"
endif
	@echo "Virtual environment created. Now you can run 'make install'."
