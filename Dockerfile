# Use the official Python base image
FROM python:3.11-slim as builder

# Set the working directory inside the container


WORKDIR /app

COPY requirements.txt .

RUN python -m venv /opt/venv
RUN /opt/venv/bin/pip install --upgrade pip && \
    /opt/venv/bin/pip install -r requirements.txt

FROM python:3.10.13-slim

ENV TZ=Asia/Almaty
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

RUN adduser --disabled-password --gecos '' myuser

WORKDIR /app

COPY --from=builder /opt/venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"

COPY . .

USER myuser

CMD ["alembic", "upgrade", "head", "&&", "uvicorn", "app.main:app", "--reload", "--host", "127.0.0.1", "--port", "8000"]