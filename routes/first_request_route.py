from typing import Dict


from fastapi import APIRouter, Depends, HTTPException, Request
from fastapi.responses import JSONResponse
# from kafka import KafkaConsumer, KafkaProducer
from psycopg2.extras import Json

from db.db import connect_to_Google_Cloud_SQL

from .functions.auth.tokens import verify_token

router = APIRouter()

@router.post("/update_profile")
async def update_profile(request: Request, token: str = Depends(verify_token)):
    try:
        data = await request.json()
        iinbin = token["sub"]


        if data and len(data):
            conn = connect_to_Google_Cloud_SQL()
            cursor = conn.cursor()
            cursor.callproc('post_profile_edit_application', [iinbin, (Json(data))])
            result = cursor.fetchone()[0]
            conn.commit()

            if result != 'Success':
                raise HTTPException(status_code=401, detail=result)
            return {"message": result}
        else:
            raise HTTPException(status_code=401, detail="No changes found")
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=401)
    finally:
        try:
            conn.close()
        except:
            print('Connection already closed')



@router.get("/client_requests")
async def get_client_requests(token: str = Depends(verify_token)):
    try:
        iinbin = token["sub"]

        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('get_profile_app_client', [iinbin])
        result = cursor.fetchone()[0]

        if not result:
            raise HTTPException(status_code=401, detail="Profile not found")

        return result
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=401)
    finally:
        try:
            conn.close()
        except:
            print('Connection already closed')



@router.get("/manager_requests")
async def get_manager_requests(token: str = Depends(verify_token)):
    try:
        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('get_profile_app_manager')
        result = cursor.fetchone()[0]

        if not result:
            raise HTTPException(status_code=401, detail="Profile not found")


        return result
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=401)
    finally:
        try:
            conn.close()
        except:
            print('Connection already closed')



@router.post("/manager_requests_detail")
async def get_manager_requests_detail(request: Request, token: str = Depends(verify_token)):
    try:
        data = await request.json()
        id_app = data.get("id_app")

        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('get_profile_app_detail_manager', [id_app])
        result = cursor.fetchone()[0]

        return result
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=401)
    finally:
        try:
            conn.close()
        except:
            print('Connection already closed')


@router.post("/manager_requests_response")
async def manager_response(request: Request, token: str = Depends(verify_token)):
    try:
        data = await request.json()
        id_app = data.get("id_app")
        status = int(data.get("status"))

        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('post_manager_response', [id_app, status])
        conn.commit()
        result = cursor.fetchone()[0]
        return result
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=401)
    finally:
        try:
            conn.close()
        except:
            print('Connection already closed')

