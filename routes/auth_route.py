from typing import Dict

from fastapi import APIRouter, Depends, HTTPException, Request
from fastapi.responses import JSONResponse

from db.db import connect_to_Google_Cloud_SQL

from .functions.auth.century_and_gender import get_century_and_gender
from .functions.auth.tokens import create_token, revoked_tokens, verify_token


router = APIRouter()

@router.post("/register")
async def register(request: Request):
    try:# iinbin: str = Body(...), password: str = Body(...), full_name: str = Body(...), birthdate: str = Body(...)
        role = "client"
        data = await request.json()
        iinbin = data.get("iinbin")
        print(data)
        year = int(iinbin[:2])
        month = iinbin[2:4]
        day = iinbin[4:6]
        century_and_gender_num = int(iinbin[6])
        century_and_gender = get_century_and_gender(century_and_gender_num)
        year = year + century_and_gender["century"]
        gender = century_and_gender["gender"]
        birthdate = f"{year}-{month}-{day}"
        password = data.get("password")
        fullname = data.get("name")
        birthplace = data.get("birthplace")
        nation = data.get("nation")

        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc(
            "signup",
            [iinbin, password, role, fullname, birthdate, gender, birthplace, nation],
        )
        message = cursor.fetchone()[0]
        conn.commit()
        
        if message != 'Success':
            raise HTTPException(status_code=401, detail=message)

        return {"message": "Account created successfully"}
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=401)
    finally:
        try:
            conn.close()
        except:
            print('Connection already closed')



@router.post("/login")
async def login(request: Request):
    try:
        data = await request.json()
        iinbin = data.get("iinbin")
        password = data.get("password")

        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('login', [iinbin, password])
        message = cursor.fetchone()
        conn.commit()

        if message[0] != "Success":
            raise HTTPException(status_code=401, detail=message[1])

        token_data = {"sub": iinbin}
        access_token = create_token(token_data)
        if iinbin in revoked_tokens:
            revoked_tokens.discard(iinbin)

        response_data = {"access_token": access_token, "token_type": "bearer", "role" : message[1]}
        return JSONResponse(content=response_data, status_code=200)
    except Exception as e:
            return JSONResponse(content={"error": str(e)}, status_code=401)
    finally:
        try:
            conn.close()
        except:
            print('Connection already closed')



@router.post("/logout")
async def logout(token: str = Depends(verify_token)):
    revoked_tokens.add(token["sub"])
    return {"message": "Logout successful"}



@router.get("/profile", response_model=dict)
async def profile(token: str = Depends(verify_token)):
    try:
        iinbin = token["sub"]

        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('get_profile_info', [iinbin])
        user_info = cursor.fetchone()[0]

        if not user_info:
            raise HTTPException(status_code=401, detail="User not found")

        for key, value in user_info.items():
            if value == 'None':
                user_info[key] = 'Нет данных'

        return user_info
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=401)
    finally:
        try:
            conn.close()
        except:
            print('Connection already closed')

