import asyncio

from typing import Dict

from fastapi import APIRouter, Depends, Request
from fastapi.responses import JSONResponse

from db.db import connect_to_Google_Cloud_SQL

from .functions.auth.tokens import verify_token
from .functions.second_request.get_samruk_list import get_samruk_list
from .functions.second_request.get_data_by_bin import get_data_by_bin
from .functions.kafka.methods import background_task

router = APIRouter()

@router.post("/check_company")
async def check_company(request: Request, token: str = Depends(verify_token)): # , token: str = Depends(verify_token)
    try:
        data = await request.json()
        iinbin = data.get("iinbin")
        
        if len(iinbin) != 12:
            return {"success": False}
        
        stat_data = get_data_by_bin(iinbin)
        
        if stat_data["success"]:
            company_name = stat_data["obj"]["name"]
            return {"success": True, "company_name": company_name}


        else:    
            info = get_samruk_list()
            companies = info["Objects"]
            companyname = ''
            for company in companies:
                if company.get("flBin") == iinbin:
                    companyname = company
                    return {"success": True, "company_name": companyname["flNameRu"]}
        
    
        return {"success": False}
    
    except Exception as e:
        return JSONResponse(content={"error": str(e)}, status_code=401)

   

@router.post("/submit_application")
async def submit_application(request: Request, token: str = Depends(verify_token)):            
    try:
        data = await request.json()
        iinbin_person = token["sub"]
        data['iinbin_person'] = iinbin_person
        
        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('get_profile_info', [iinbin_person])
        user_info = cursor.fetchone()[0]

        client_email = user_info["email"]

        task = asyncio.create_task(background_task(data, client_email))
        await task
       
        
        return {"message": "Application submitted successfully"}

    
    except Exception as e:
        return {"error": f"An error occurred during application submission: {str(e)}"}

    
