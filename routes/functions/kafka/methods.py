import os

from fastapi import HTTPException

from psycopg2.extras import Json

from db.db import connect_to_Google_Cloud_SQL

from vahfka import KafkaLibrary, KafkaProducerWithCallbacks, KafkaConsumerWrapper
from ..second_request.get_data_by_bin import get_data_by_bin
from ..second_request.pdf_generator import generate_pdf
from ..second_request.telegram import TelegramBot, get_chat_id


kafka_bootstrap_servers = os.getenv("KAFKA_BROKER")
kafka_library = KafkaLibrary(bootstrap_servers=kafka_bootstrap_servers)

kafka_consumer_iinbin = KafkaConsumerWrapper(
    bootstrap_servers=kafka_bootstrap_servers,
    group_id=os.getenv("KAFKA_GROUP1"),
    topics=[os.getenv("KAFKA_TOPIC1")]
)
kafka_consumer_company = KafkaConsumerWrapper(
    bootstrap_servers=kafka_bootstrap_servers,
    group_id=os.getenv("KAFKA_GROUP2"),
    topics=[os.getenv("KAFKA_TOPIC2")]
)

telegram_bot = TelegramBot()
telegram_bot.run()

def delivery_report(err, msg):
    if err is not None:
        print(f'Message delivery failed: {err}')


async def background_task(data: dict, client_email: str) -> None:
    try:
        iinbin = data.get("iinbin")
        company_name = data.get("company_name")
        iinbin_person = data.get("iinbin_person")

        kafka_data = {"iinbin": iinbin, "name": company_name}
        
        with KafkaProducerWithCallbacks(bootstrap_servers=kafka_bootstrap_servers) as producer:
            producer.send_message_async(topic='iinbintopic', message=kafka_data)
        
        consume_messages()
        company_info = consume_messages2()
        print(company_info)
        generate_pdf(company_info, iinbin, client_email)

        chat_id = get_chat_id(iinbin_person)

        if not iinbin or not chat_id:
            raise HTTPException(status_code=400, detail="Missing iinbin or chat_id.")

        await telegram_bot.create_and_send_pdf(chat_id, company_info, iinbin)
        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('post_company_info', [Json(company_info)])
        conn.commit()

    except Exception as e:
        print(f"An error occurred during background_task: {str(e)}")


def consume_messages():
    messages = kafka_consumer_iinbin.consume_messages()
    process_iinbin(messages[-1]['iinbin'])
    # print((messages[0]['iinbin']))
    # for message in messages:
    #     process_iinbin(message)

def consume_messages2():
    messages = kafka_consumer_company.consume_messages()
    if messages:
        # Process the message as needed
        latest_message = messages[-1]
        # print(f"Received message from companytopic: {latest_message}")
        return latest_message



def process_iinbin(message):
    try:
        iinbin_data = message
        conn = connect_to_Google_Cloud_SQL()
        cursor = conn.cursor()
        cursor.callproc('check_date', [iinbin_data])
        result = cursor.fetchone()[0]
        if result['data'] != 0:
            company_params = result['data']
        else:
            company_info = get_data_by_bin(iinbin_data)
            company_params = company_info["obj"]
        with KafkaProducerWithCallbacks(bootstrap_servers=kafka_bootstrap_servers) as producer:
            producer.send_message_async(topic='companytopic', message=company_params)
        print(f"Processed and saved data for BIN: {iinbin_data}")
    except Exception as e:
        print(f"Error processing message companytopic: {str(e)}")

