import json
import os

def load_data_from_file(file_path):

    with open(file_path, 'r', encoding='utf-8') as file:
        data = json.load(file)
    return data


def get_data_from_egov():

    script_directory = os.path.dirname(os.path.abspath(__file__))
    file_path = os.path.join(script_directory, '..', '..', '../storage', 'data_from_egov.json')
    
    return load_data_from_file(file_path)

