import os
import smtplib
from datetime import datetime
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from decouple import config
from fpdf import FPDF


def generate_pdf(company_info, iinbin, client_email):
    class PDFWithFrame(FPDF):
        def header(self):
            self.add_frame()

        def footer(self):
            pass  # No footer for now, you can customize it if needed

        def add_frame(self):
            margin = 10
            self.rect(margin, margin, self.w - 2 * margin, self.h - 2 * margin)

    pdf = PDFWithFrame()
    pdf.add_page()

    script_directory = os.path.dirname(os.path.abspath(__file__))

    # Ensure the "PDFs" folder exists in the "static" directory
    pdfs_folder = os.path.join(script_directory, "..", "..", "../static", "PDFs")

    os.makedirs(pdfs_folder, exist_ok=True)

    font_path = os.path.join(script_directory, "..", "..", "../font", "DejaVuSans.ttf")
    pdf.add_font("DejaVuSans", '', font_path, uni=True)
    pdf.set_font("DejaVuSans", '', size=16)
    pdf.set_left_margin(15)
    pdf.set_right_margin(15)

    pdf.ln()
    pdf.set_font("DejaVuSans", '', size=20)
    pdf.cell(200, 10, txt="Company Information", ln=True, align='C')
    pdf.ln()
    pdf.set_font("DejaVuSans", size=12)
    pdf.set_fill_color(200, 220, 255)
    col_widths = [60, 90]
    headers = ["Attribute", "Value"]

    for header, width in zip(headers, col_widths):
        pdf.cell(width, 10, txt=header, border=1, fill=True)

    pdf.ln()

    pdf.set_draw_color(0, 0, 0)
    pdf.set_fill_color(255, 255, 255)
    pdf.set_line_width(0.2)
    image_path = os.path.join(script_directory, "..", "..", "../static", "sign.jpg")
    for key, value in company_info.items():
        value = str(value) if value is not None else ""
        pdf.cell(col_widths[0], 10, txt=key.capitalize(), border=1)
        pdf.multi_cell(col_widths[1], 10, txt=value, border=1)

    bottom_margin = 20  # Adjust the bottom margin as needed
    pdf.image(image_path, x=pdf.w - 50, y=pdf.h - 50 - bottom_margin, w=30)

    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")

    output_filename = os.path.join(pdfs_folder, f"{iinbin}_{timestamp}.pdf")

    pdf.output(output_filename)

    send_email(client_email, "Информация о компании", f"Отправляем PDF-файл с информацией о компании, чей БИН:  {iinbin}.", output_filename)


def send_email(to_email, subject, body, attachment_path):
    # Your Gmail account credentials
        smtp_username = config('SMTP_USERNAME')
        smtp_password = config('SMTP_PASSWORD')

        msg = MIMEMultipart()
        msg["From"] = smtp_username
        msg["To"] = to_email
        msg["Subject"] = subject

        # Attach the PDF file
        with open(attachment_path, "rb") as file:
            attach = MIMEApplication(file.read(), _subtype="pdf")
            attach.add_header('Content-Disposition', 'attachment', filename=os.path.basename(attachment_path))
            msg.attach(attach)

        # Add the body text
        msg.attach(MIMEText(body, "plain"))

        try:
            # Connect to Gmail's SMTP server
            with smtplib.SMTP("smtp.gmail.com", 587) as server:
                server.starttls()
                server.login(smtp_username, smtp_password)
                
                # Send the email
                server.sendmail(smtp_username, to_email, msg.as_string())
                print("Email sent successfully!")
        except Exception as e:
            print(f"An error occurred: {e}")



def generate_pdf_tg(company_info, iinbin):
    class PDFWithFrame(FPDF):
        def header(self):
            self.add_frame()

        def footer(self):
            pass  # No footer for now, you can customize it if needed

        def add_frame(self):
            margin = 10
            self.rect(margin, margin, self.w - 2 * margin, self.h - 2 * margin)

    pdf = PDFWithFrame()
    pdf.add_page()

    script_directory = os.path.dirname(os.path.abspath(__file__))

    # Ensure the "PDFs" folder exists in the "static" directory
    pdfs_folder = os.path.join(script_directory, "..", "..", "../static", "PDFs")
    
    os.makedirs(pdfs_folder, exist_ok=True)

    font_path = os.path.join(script_directory, "..", "..", "../font", "DejaVuSans.ttf")
    pdf.add_font("DejaVuSans", '', font_path, uni=True)
    pdf.set_font("DejaVuSans", '', size=16)
    pdf.set_left_margin(15)
    pdf.set_right_margin(15)

    pdf.ln()
    pdf.set_font("DejaVuSans", '', size=20)
    pdf.cell(200, 10, txt="Company Information", ln=True, align='C')
    pdf.ln()
    pdf.set_font("DejaVuSans", size=12)
    pdf.set_fill_color(200, 220, 255)
    col_widths = [60, 90]
    headers = ["Attribute", "Value"]

    for header, width in zip(headers, col_widths):
        pdf.cell(width, 10, txt=header, border=1, fill=True)

    pdf.ln()

    pdf.set_draw_color(0, 0, 0)
    pdf.set_fill_color(255, 255, 255)
    pdf.set_line_width(0.2)
    image_path = os.path.join(script_directory, "..", "..", "../static", "sign.jpg")
    
    
    for key, value in company_info.items():
        value = str(value) if value is not None else ""
        pdf.cell(col_widths[0], 10, txt=key.capitalize(), border=1)
        pdf.multi_cell(col_widths[1], 10, txt=value, border=1)

    bottom_margin = 20  # Adjust the bottom margin as needed
    pdf.image(image_path, x=pdf.w - 50, y=pdf.h - 50 - bottom_margin, w=30)

    timestamp = datetime.now().strftime("%Y%m%d%H%M%S")

    output_filename = os.path.join(pdfs_folder, f"{iinbin}_{timestamp}.pdf")

    pdf.output(output_filename)

    return output_filename

