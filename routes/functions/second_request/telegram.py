import os

import requests
from telegram import Bot, InputFile, Update, InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CommandHandler, Updater, MessageHandler, Filters, ConversationHandler, CallbackQueryHandler, CallbackContext

from .pdf_generator import generate_pdf_tg
from db.db import connect_to_Google_Cloud_SQL

def get_chat_id(iin):
    conn = connect_to_Google_Cloud_SQL()
    cursor = conn.cursor()
    print(iin)
    cursor.callproc('get_chat_id', [iin])
    result = int(cursor.fetchone()[0])
    print(result)
    conn.close()
    return result

def save_chat_id(iin, password, chat_id):
    conn = connect_to_Google_Cloud_SQL()
    cursor = conn.cursor()
    print(iin, password)
    cursor.callproc('save_chat_id', [iin, password, chat_id])
    conn.commit()
    result = cursor.fetchone()[0]
    print(result)
    conn.close()
    if result == 'Success':
        return True
    return False

def check_auth(iin, password):
    conn = connect_to_Google_Cloud_SQL()
    cursor = conn.cursor()
    print(iin, password)
    cursor.callproc('login', [iin, password])
    result = cursor.fetchone()
    print(result)
    conn.close()
    if result[0] == 'Success' and result[1] == 'client':
        return True
    return False

class TelegramBot:
    GET_IIN, GET_PASSWORD, HANDLE_CHANGE_CHAT_ID, HANDLE_SAVE_CHAT_ID = range(4)

    def __init__(self):
        self.bot_token = os.getenv("TELEGRAM_BOT_TOKEN")
        self.bot = Bot(token=self.bot_token)

        self.updater = Updater(token=self.bot_token, use_context=True)
        self.dispatcher = self.updater.dispatcher

        conversation_handler = ConversationHandler(
            entry_points=[CommandHandler('sign_in', self.sign_in)],
            states={
                self.GET_IIN: [MessageHandler(Filters.text & ~Filters.command, self.get_iin)],
                self.GET_PASSWORD: [MessageHandler(Filters.text & ~Filters.command, self.get_password)],
                self.HANDLE_CHANGE_CHAT_ID: [CallbackQueryHandler(self.handle_change_chat_id, False)],
                self.HANDLE_SAVE_CHAT_ID: [CallbackQueryHandler(self.handle_save_chat_id, False)],
            },
            fallbacks=[],
        )
        self.dispatcher.add_handler(conversation_handler)

    def sign_in(self, update: Update, context: CallbackContext):
        print('sign_in')
        chat_id = update.message.chat_id
        context.bot.send_message(chat_id, "Please enter your IIN (12 digits):")
        return self.GET_IIN

    def get_iin(self, update: Update, context: CallbackContext):
        print('get_iin')
        iin = update.message.text
        print(type(update.message))
        if len(iin) == 12 and iin.isdigit():
            context.user_data['iin'] = iin
            context.bot.send_message(update.message.chat_id, "Please enter your password:")
            return self.GET_PASSWORD
        else:
            context.bot.send_message(update.message.chat_id, "Invalid IIN. Please enter a valid 12-digit IIN.")
            return self.GET_IIN

    def get_password(self, update: Update, context: CallbackContext):
        print('get_password')
        password = update.message.text
        print(type(update.message))
        if password:
            context.user_data['password'] = password
            if check_auth(iin=context.user_data['iin'], password=context.user_data['password']):
                if get_chat_id(iin=context.user_data['iin']):
                    keyboard = [
                        [InlineKeyboardButton("Yes", callback_data='change_chat_id')],
                        [InlineKeyboardButton("No", callback_data='do_not_change_chat_id')],
                    ]
                    reply_markup = InlineKeyboardMarkup(keyboard)
                    context.bot.send_message(update.message.chat_id, f"We already have account to which we can send the result of your application. Do you want to change chat ID?", reply_markup=reply_markup)
                    return self.HANDLE_CHANGE_CHAT_ID
                else:
                    keyboard = [
                        [InlineKeyboardButton("Yes", callback_data='save_chat_id')],
                        [InlineKeyboardButton("No", callback_data='do_not_save_chat_id')],
                    ]
                    reply_markup = InlineKeyboardMarkup(keyboard)
                    context.bot.send_message(update.message.chat_id, "Unfortunatelly, we could not find registered telegram account by your Egov profile. Do you want to save it?", reply_markup=reply_markup)
                    return self.HANDLE_SAVE_CHAT_ID
            else:
                context.bot.send_message(update.message.chat_id,"Invalid login or password")
                return ConversationHandler.END
        else:
            context.bot.send_message(update.message.chat_id, "Password cannot be empty. Please enter a password.")
            return self.GET_PASSWORD

    def handle_change_chat_id(self, update: Update, context: CallbackContext):
        query = update.callback_query
        chat_id = update.message.chat_id if update.message else update.effective_chat.id

        if query.data == 'change_chat_id':
            if save_chat_id(iin=context.user_data['iin'], password=context.user_data['password'], chat_id=chat_id):
                context.bot.send_message(chat_id, "Chat ID changed successfully.")
            else:
                context.bot.send_message(chat_id, "Catched error while changing Chat ID")
        elif query.data == 'do_not_change_chat_id':
            context.bot.send_message(chat_id, "Chat ID not changed.")
        return ConversationHandler.END  # End the conversation

    def handle_save_chat_id(self, update: Update, context: CallbackContext):
        print('handle_save_chat_id')
        query = update.callback_query
        chat_id = update.message.chat_id if update.message else update.effective_chat.id

        if query.data == 'save_chat_id':
            if save_chat_id(iin=context.user_data['iin'], password=context.user_data['password'], chat_id=chat_id):
                context.bot.send_message(chat_id, "Chat ID saved successfully.")
            else:
                context.bot.send_message(chat_id, "Catched error while saving Chat ID")
        elif query.data == 'do_not_save_chat_id':
            print('handle_save_chat_id false')
            context.bot.send_message(chat_id, "Chat ID not saved.")
        return ConversationHandler.END  # End the conversation

    def run(self):
        self.updater.start_polling()
        self.updater.idle()

    async def send_pdf(self, chat_id, pdf_path):
        with open(pdf_path, "rb") as pdf_file:
            await self.bot.send_document(
                chat_id, document = InputFile(pdf_file), caption="Your PDF Document"
            )

    async def create_and_send_pdf(self, chat_id, company_info, iinbin):
        pdf_path = generate_pdf_tg(company_info, iinbin)
        await self.send_pdf(chat_id, pdf_path)
        return pdf_path
