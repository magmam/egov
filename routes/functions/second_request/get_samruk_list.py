import requests, time, os

def get_samruk_list():
    stat_api_url = os.getenv("SAMRUK_LIST")
    max_retries = 3
    current_retry = 0
    while current_retry < max_retries:
        try:
            response = requests.get(stat_api_url)
            response.raise_for_status()  # Raise an exception for non-2xx responses
            stat_data = response.json()
            return stat_data
        except requests.exceptions.HTTPError as errh:
            if response.status_code == 429:
                # Retry after an increasing amount of time
                retry_after = int(response.headers.get('Retry-After', 5))
                print(f"Rate limited. Retrying after {retry_after} seconds...")
                time.sleep(retry_after)
                current_retry += 1
            else:
                print(f"HTTP Error: {errh}")
                break
        except requests.exceptions.RequestException as err:
            print(f"Request Exception: {err}")
            break

    print("Max retries reached. Unable to get data.")
    return None


