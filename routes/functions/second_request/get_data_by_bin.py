from .data_from_egov import get_data_from_egov
from .get_samruk_list import get_samruk_list
from .functions import find_entry_by_bin
import time, requests


def get_data_by_bin(iinbin):

    stat_api_url = f"https://old.stat.gov.kz/api/juridical/counter/api/?bin={iinbin}&lang=ru"
    
    # Set the maximum number of retries
    max_retries = 3
    current_retry = 0

    while current_retry < max_retries:
        try:
            response = requests.get(stat_api_url)
            response.raise_for_status()  # Raise an exception for non-2xx responses
            stat_data = response.json()

            samruk_info = get_samruk_list()
            data_from_egov = get_data_from_egov()
            companies = samruk_info["Objects"]
            
            flOpf = "Нет данных"
            flStateInvolvement = "Нет данных"
            flStatus = "Нет данных"

            for company in companies:
                if company.get("flBin") == iinbin:
                    flOpf = company.get('flOpf')
                    flStateInvolvement = company.get('flStateInvolvement')
                    flStatus = company.get('flStatus')
            gosSector = find_entry_by_bin(data_from_egov, iinbin)

            stat_data['obj']['flOpf'] = flOpf
            stat_data['obj']['flStateInvolvement'] = flStateInvolvement
            stat_data['obj']['flStatus'] = flStatus
            stat_data['obj']['gosSector'] = gosSector

            return stat_data
        

        except requests.exceptions.HTTPError as errh:
            if response.status_code == 429:
                # Retry after an increasing amount of time
                retry_after = int(response.headers.get('Retry-After', 5))
                print(f"Rate limited. Retrying after {retry_after} seconds...")
                time.sleep(retry_after)
                current_retry += 1
            else:
                print(f"HTTP Error: {errh}")
                break
        except requests.exceptions.RequestException as err:
            print(f"Request Exception: {err}")
            break

    print("Max retries reached. Unable to get data.")
    return None