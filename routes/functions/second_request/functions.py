def find_entry_by_bin(data, iinbin):
    for entry in data:
        if entry.get('bin') == iinbin:
            return 1
    return 0

