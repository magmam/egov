def get_century_and_gender(num: int):
    if(num == 1):
        return {
                "century": 1800,
                "gender" : 1
                }
    elif(num == 2):
        return {
                "century": 1800,
                "gender" : 2
                }
    elif(num == 3):
        return {
                "century": 1900,
                "gender" : 1
                }
    elif(num == 4):
        return {
                "century": 1900,
                "gender" : 2
                }
    elif(num == 5):
        return {
                "century": 2000,
                "gender" : 1
                }
    elif(num == 6):
        return {
                "century": 2000,
                "gender" : 2
                }
