CREATE TABLE IF NOT EXISTS public.auth
(
    iin_bin character(12) COLLATE pg_catalog."default" NOT NULL,
    password character varying(255) COLLATE pg_catalog."default" NOT NULL,
    load_dttm timestamp without time zone DEFAULT now(),
    latest_login_dttm timestamp without time zone DEFAULT now(),
    role character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT auth_pkey PRIMARY KEY (iin_bin)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.auth
    OWNER to postgres;