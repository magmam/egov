CREATE TABLE IF NOT EXISTS public.application_status
(
    id integer NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    rowstatus bit(1) DEFAULT (1)::bit(1),
    load_dttm timestamp without time zone DEFAULT now(),
    CONSTRAINT application_status_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.application_status
    OWNER to postgres;