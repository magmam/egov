CREATE OR REPLACE FUNCTION public.get_chat_id(
	iin_bin_val character)
    RETURNS integer
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
	IF NOT EXISTS(SELECT 1 FROM profile WHERE iin_bin = iin_bin_val) THEN
		RETURN 0;
	END IF;
	RETURN (SELECT chat_id FROM profile WHERE iin_bin = iin_bin_val LIMIT 1);
END;
$BODY$;