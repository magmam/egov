CREATE TYPE public.res AS
(
	status character varying,
	message character varying
);

ALTER TYPE public.res
    OWNER TO postgres;

CREATE OR REPLACE FUNCTION public.login(
	iin_bin_val character varying,
	password_val character varying)
    RETURNS res
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    role_val VARCHAR(255);
	response res;
BEGIN
    IF EXISTS(SELECT 1 FROM auth WHERE iin_bin = iin_bin_val AND password = encode(digest(password_val, 'sha256'), 'hex')) THEN
        UPDATE auth SET latest_login_dttm = NOW()
        WHERE iin_bin = iin_bin_val;
		
		role_val := (SELECT role from auth WHERE iin_bin = iin_bin_val);
		
		response := ('Success', role_val);
    ELSIF EXISTS(SELECT 1 FROM auth WHERE iin_bin = iin_bin_val) THEN
        response := ('Incorrect password', '');
    ELSE
        response := ('IIN or BIN does not exist', '');
    END IF;
	
	RETURN response;
END;
$BODY$;

ALTER FUNCTION public.login(character varying, character varying)
    OWNER TO postgres;