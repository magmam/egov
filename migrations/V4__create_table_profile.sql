CREATE TABLE IF NOT EXISTS public.profile
(
    iin_bin character(12) COLLATE pg_catalog."default" NOT NULL,
    birth_date date,
    birth_place character varying(255) COLLATE pg_catalog."default",
    nationality character varying(255) COLLATE pg_catalog."default",
    sex character(1) COLLATE pg_catalog."default",
    email character varying(255) COLLATE pg_catalog."default",
    phone_number character varying(255) COLLATE pg_catalog."default",
    education character varying(255) COLLATE pg_catalog."default",
    current_address character varying(255) COLLATE pg_catalog."default",
    rowstatus bit(1) DEFAULT (1)::bit(1),
    load_dttm timestamp without time zone DEFAULT now(),
    last_update_dttm timestamp without time zone DEFAULT now(),
    fio character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT profile_pkey PRIMARY KEY (iin_bin)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.profile
    OWNER to postgres;