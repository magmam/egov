CREATE OR REPLACE FUNCTION public.get_profile_app_detail_manager(
	id_val character)
    RETURNS SETOF json 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
    profile_data RECORD;
    result_list json[];
BEGIN
	IF NOT EXISTS(SELECT 1 from profile_applications WHERE id = id_val) THEN
		RETURN QUERY SELECT json_build_object('data', 'Application id does not exist')::json;
        RETURN;
	END IF;
	
	FOR profile_data IN
        SELECT (SELECT name_front FROM profile_columns WHERE id = pad.profile_columns_id) as column,
               pad.old_value, 
               pad.new_value
        FROM profile_application_details pad
		WHERE pad.profile_applications_id = id_val
    LOOP
        result_list := result_list || json_build_object(
			'column', profile_data.column,
        	'old_value', COALESCE(profile_data.old_value, 'Нет данных'),
        	'new_value', COALESCE(profile_data.new_value, 'Нет данных')
		);
	END LOOP;

    RETURN QUERY SELECT json_build_object('data', result_list);
END;
$BODY$;

ALTER FUNCTION public.get_profile_app_detail_manager(character)
    OWNER TO postgres;