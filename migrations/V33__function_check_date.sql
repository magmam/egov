CREATE OR REPLACE FUNCTION public.check_date(
	iin_bin_val character)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

DECLARE
    response json;
BEGIN
	IF NOT EXISTS(SELECT * from company_info WHERE bin = iin_bin_val and NOW() between BEGIN_DATE and END_DATE) THEN
		return json_build_object('data', 0);
	END IF;
	
	SELECT json_build_object(
		'bin',bin,
    	'name',name,
    	'registerDate',registerDate,
    	'okedCode',okedCode,
    	'okedName',okedName,
    	'secondOkeds',secondOkeds,
    	'krpCode',krpCode,
    	'krpName',krpName,
    	'krpBfCode',krpBfCode,
    	'krpBfName',krpBfName,
		'kseCode',kseCode,
		'kseName',kseName,
		'kfsCode',kfsCode,
		'kfsName',kfsName,
    	'katoCode',katoCode, 
		'katoId',katoId,
    	'katoAddress',katoAddress, 
    	'fio',fio,
    	'ip',ip,
		'flOpf',flOpf,
    	'flStateInvolvement',flStateInvolvement, 
    	'flStatus',flStatus,
    	'gosSector',gosSector
    ) INTO response
    FROM company_info
    WHERE bin = iin_bin_val and NOW() between BEGIN_DATE and END_DATE;

    RETURN json_build_object('data', response);
END;
$BODY$;