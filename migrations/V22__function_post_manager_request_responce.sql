CREATE OR REPLACE FUNCTION public.post_manager_requests_response(
	id_val character,
	status_val integer)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
	UPDATE profile_applications
	SET application_status_id = status_val
	WHERE id = id_val;
	
	return;
END;
$BODY$;

ALTER FUNCTION public.post_manager_requests_response(character, integer)
    OWNER TO postgres;