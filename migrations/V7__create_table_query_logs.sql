CREATE TABLE IF NOT EXISTS public.query_logs
(
    id integer NOT NULL DEFAULT nextval('query_logs_id_seq'::regclass),
    sql_query text COLLATE pg_catalog."default",
    create_dttm timestamp without time zone DEFAULT now(),
    CONSTRAINT query_logs_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.query_logs
    OWNER to postgres;