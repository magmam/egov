CREATE OR REPLACE FUNCTION public.post_profile_edit_application(
	iin_bin_val character,
	edits json)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    profile_applications_id_val char(14);
	column_val varchar(255);
	new_value_val varchar(255);
	query varchar;
	cnt int := 0;
BEGIN
	IF NOT EXISTS(SELECT 1 FROM profile WHERE iin_bin = iin_bin_val) THEN
		RETURN 'Such IIN or BIN does not exists';
	END IF;
	
	IF EXISTS(SELECT 1 FROM profile_applications WHERE iin_bin = iin_bin_val and application_status_id = 1) THEN
		RETURN 'You have active application, try again later';
	END IF;
	
	IF edits IS NULL THEN
		RETURN 'Empty input json';
	END IF;
	
	profile_applications_id_val := (SELECT CONCAT(TO_CHAR(NOW(), 'YYYYMMDD'), LPAD(CAST((SELECT count(*) FILTER(WHERE TO_CHAR(create_dttm, 'YYYYMMDD') = TO_CHAR(NOW(), 'YYYYMMDD')) FROM profile_applications) + 1 AS varchar(255)), 6, '0')));
	
	BEGIN
    	INSERT INTO profile_applications (id, iin_bin)
    	VALUES (profile_applications_id_val, iin_bin_val);
    	    
		FOR column_val, new_value_val IN SELECT * FROM json_each_text(edits)
  		LOOP
			IF EXISTS (SELECT 1 FROM profile_columns WHERE name = column_val and is_editable = CAST(1 as bit)) THEN
				query := format('
					INSERT INTO profile_application_details(profile_applications_id, profile_columns_id, old_value, new_value)
					SELECT %L as profile_applications_id, %L as profile_columns_id, %I as old_value, %L as new_value
					FROM profile
					WHERE iin_bin = %L and rowstatus = CAST(1 as bit) and (%I IS NULL or %I <> %L)'
				, profile_applications_id_val, (SELECT id FROM profile_columns WHERE name = column_val),column_val, new_value_val,  iin_bin_val, column_val, column_val, new_value_val);
				
				INSERT INTO query_logs(sql_query) VALUES (query);
				
				EXECUTE query;
				cnt := cnt + 1;
			END IF;
				
		END LOOP;
    	    
		if cnt = 0 THEN
			ROLLBACK;
            RETURN 'No data updated';
		else
			RETURN 'Success';
		end if;
		
		--COMMIT;
	EXCEPTION
    	WHEN others THEN
    	    RETURN 'Error';
		END;
END;
$BODY$;

ALTER FUNCTION public.post_profile_edit_application(character, json)
    OWNER TO postgres;