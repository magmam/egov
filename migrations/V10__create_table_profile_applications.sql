CREATE TABLE IF NOT EXISTS public.profile_applications
(
    id character(14) COLLATE pg_catalog."default" NOT NULL,
    iin_bin character(12) COLLATE pg_catalog."default" NOT NULL,
    application_status_id integer NOT NULL DEFAULT 1,
    create_dttm timestamp without time zone NOT NULL DEFAULT now(),
    close_dttm timestamp without time zone,
    CONSTRAINT profile_applications_pkey PRIMARY KEY (id),
    CONSTRAINT profile_applications_application_status_id_fkey FOREIGN KEY (application_status_id)
        REFERENCES public.application_status (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT profile_applications_iin_bin_fkey FOREIGN KEY (iin_bin)
        REFERENCES public.profile (iin_bin) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.profile_applications
    OWNER to postgres;