CREATE OR REPLACE TRIGGER update_profile_trigger
    BEFORE UPDATE 
    ON public.profile_applications
    FOR EACH ROW
    WHEN (old.application_status_id IS DISTINCT FROM new.application_status_id)
    EXECUTE FUNCTION public.update_profile_on_change();
