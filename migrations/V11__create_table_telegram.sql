CREATE TABLE IF NOT EXISTS public.telegram
(
    iin character(12)[] COLLATE pg_catalog."default" NOT NULL,
    chat_id character(1)[] COLLATE pg_catalog."default",
    CONSTRAINT telegram_pkey PRIMARY KEY (iin)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.telegram
    OWNER to postgres;