CREATE OR REPLACE FUNCTION public.post_company_info(
	data json)
    RETURNS void
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
	bin_val char(12);
    name_val varchar(255);
    registerDate_val timestamp;
    okedCode_val varchar(255);
    okedName_val varchar(255);
    secondOkeds_val varchar(255);
    krpCode_val varchar(255);
    krpName_val varchar(255);
    krpBfCode_val varchar(255);
    krpBfName_val varchar(255);
    kseCode_val varchar(255);
    kseName_val varchar(255);
    kfsCode_val varchar(255);
    kfsName_val varchar(255);
    katoCode_val varchar(255);
    katoId_val integer;
    katoAddress_val varchar(255);
    fio_val varchar(255);
    ip_val bool;
BEGIN
	bin_val := data->>'bin';
    name_val := data->>'name';
    registerDate_val := data->>'registerDate';
    okedCode_val := data->>'okedCode';
    okedName_val := data->>'okedName';
    secondOkeds_val := data->>'secondOkeds';
    krpCode_val := data->>'krpCode';
    krpName_val := data->>'krpName';
    krpBfCode_val := data->>'krpBfCode';
    krpBfName_val := data->>'krpBfName';
    kseCode_val := data->>'kseCode';
    kseName_val := data->>'kseName';
    kfsCode_val := data->>'kfsCode';
    kfsName_val := data->>'kfsName';
    katoCode_val := data->>'katoCode';
    katoId_val := (data->>'katoId')::integer;
    katoAddress_val := data->>'katoAddress';
    fio_val := data->>'fio';
    ip_val := (data->>'ip')::bool;
	
	IF EXISTS(SELECT 1 FROM company_info ci WHERE ci.bin = bin_val AND NOW() > ci.valid_to) THEN
		UPDATE company_info ci
		SET
			bin = bin_val,
			name = name_val,
    		registerDate = registerDate_val,
    		okedCode = okedCode_val,
    		okedName = okedName_val,
    		secondOkeds = secondOkeds_val,
    		krpCode = krpCode_val,
    		krpName = krpName_val,
    		krpBfCode = krpBfCode_val,
    		krpBfName = krpBfName_val,
			kseCode = kseCode_val,
			kseName = kseName_val,
			kfsCode = kfsCode_val,
			kfsName = kfsName_val,
    		katoCode = katoCode_val,
			katoId = katoId_val,
    		katoAddress = katoAddress_val,
    		fio = fio_val,
    		ip = ip_val,
			valid_fr = NOW(),
			valid_to = NOW() + INTERVAL '24 hours'
		WHERE bin_val = ci.bin;
	ELSIF NOT EXISTS(SELECT 1 FROM company_info ci WHERE bin_val = ci.bin) THEN
		INSERT INTO company_info(bin, name, registerDate, okedCode, okedName, secondOkeds, krpCode, krpName, krpBfCode, krpBfName, kseCode, kseName, kfsCode, kfsName, katoCode, katoId, katoAddress, fio, ip)
		VALUES(bin_val, name_val, registerDate_val, okedCode_val, okedName_val, secondOkeds_val, krpCode_val, krpName_val, krpBfCode_val, krpBfName_val, kseCode_val, kseName_val, kfsCode_val, kfsName_val, katoCode_val, katoId_val, katoAddress_val, fio_val, ip_val);
	END IF;
	
END;
$BODY$;

ALTER FUNCTION public.post_company_info(json)
    OWNER TO postgres;