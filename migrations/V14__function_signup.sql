CREATE OR REPLACE FUNCTION public.signup(
	iin_bin_val character varying,
	password_val character varying,
	role_val character varying,
	fio_val character varying,
	birth_date_val date,
	gender_val integer,
	birth_place_val character varying,
	nationality_val character varying)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$

BEGIN
	IF length(iin_bin_val) <> 12 or iin_bin_val NOT SIMILAR TO '[0-9]+' THEN
        RETURN 'IIN or BIN invalid';
    END IF;
	
	IF EXISTS(SELECT 1 FROM profile WHERE iin_bin = iin_bin_val) THEN
		RETURN 'Such IIN or BIN already exists';
	END IF;
	
    
    INSERT INTO auth (iin_bin, password, role)
    VALUES (iin_bin_val, encode(digest(password_val, 'sha256'), 'hex'), role_val);
    
	INSERT INTO profile(fio, iin_bin, birth_date, sex, birth_place, nationality)
	VALUES (fio_val, iin_bin_val, birth_date_val, gender_val, birth_place_val, nationality_val);
	
    RETURN 'Success';
END;
$BODY$;