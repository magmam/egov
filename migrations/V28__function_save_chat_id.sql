CREATE OR REPLACE FUNCTION public.save_chat_id(
	iin_bin_val character,
	password_val character,
	chat_id_val integer)
    RETURNS character
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
BEGIN
	IF NOT EXISTS(SELECT 1 FROM auth WHERE iin_bin = iin_bin_val and password = encode(digest(password_val, 'sha256'), 'hex')) THEN
		RETURN 'Error';
	END IF;
	
	UPDATE profile SET chat_id = chat_id_val
	WHERE iin_bin = iin_bin_val;
	
	IF EXISTS(SELECT 1 FROM profile WHERE iin_bin = iin_bin_val and chat_id = chat_id_val) THEN
		RETURN 'Success';
	END IF;
	
	RETURN 'Error';
END;
$BODY$;