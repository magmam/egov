CREATE OR REPLACE FUNCTION public.get_profile_app_manager(
	)
    RETURNS SETOF json 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
    profile_data RECORD;
    result_list json[];
BEGIN
    FOR profile_data IN
        SELECT pa.iin_bin,
               pa.id, 
               pa.create_dttm
        FROM profile_applications pa
		WHERE pa.application_status_id = 1
		ORDER BY create_dttm DESC
    LOOP
        result_list := result_list || json_build_object(
            'iin_bin', profile_data.iin_bin, 
            'id', profile_data.id, 
            'create_dttm', profile_data.create_dttm
        );
    END LOOP;

    RETURN QUERY SELECT json_build_object('data', result_list);
END;
$BODY$;

ALTER FUNCTION public.get_profile_app_manager()
    OWNER TO postgres;