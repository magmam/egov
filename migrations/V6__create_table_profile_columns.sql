CREATE TABLE IF NOT EXISTS public.profile_columns
(
    id integer NOT NULL DEFAULT nextval('profile_columns_id_seq'::regclass),
    name character varying(255) COLLATE pg_catalog."default" NOT NULL,
    is_editable bit(1) NOT NULL,
    rowstatus bit(1) DEFAULT (1)::bit(1),
    load_dttm timestamp without time zone DEFAULT now(),
    name_front character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT profile_columns_pkey PRIMARY KEY (id),
    CONSTRAINT profile_columns_name_key UNIQUE (name)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.profile_columns
    OWNER to postgres;