ALTER TABLE company_info ADD flOpf varchar(255);

ALTER TABLE company_info ADD flStateInvolvement varchar(255);

ALTER TABLE company_info ADD flStatus varchar(255);

ALTER TABLE company_info ADD gosSector bit;


ALTER TABLE company_info
RENAME COLUMN valid_to TO END_DATE;


ALTER TABLE company_info
RENAME COLUMN valid_fr TO BEGIN_DATE;