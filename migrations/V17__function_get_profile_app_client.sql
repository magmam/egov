CREATE OR REPLACE FUNCTION public.get_profile_app_client(
	iin_bin_val character)
    RETURNS SETOF json 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
DECLARE
    profile_data RECORD;
    result_list json[];
BEGIN
    FOR profile_data IN
        SELECT asa.name AS status, 
               pa.id, 
               pa.create_dttm, 
               pa.close_dttm
        FROM profile_applications pa
            INNER JOIN application_status asa ON pa.application_status_id = asa.id
        WHERE pa.iin_bin = iin_bin_val
		ORDER BY create_dttm DESC
    LOOP
        result_list := result_list || json_build_object(
            'status', profile_data.status, 
            'id', profile_data.id, 
            'create_dttm', profile_data.create_dttm, 
            'close_dttm', profile_data.close_dttm
        );
    END LOOP;

    RETURN QUERY SELECT json_build_object('data', result_list);
END;
$BODY$;

ALTER FUNCTION public.get_profile_app_client(character)
    OWNER TO postgres;