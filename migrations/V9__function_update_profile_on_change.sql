CREATE OR REPLACE FUNCTION public.update_profile_on_change()
    RETURNS trigger
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE NOT LEAKPROOF
AS $BODY$
DECLARE
	column_val varchar(255);
	new_value_val varchar(255);
	query text;
BEGIN
    IF NEW.application_status_id = 0 THEN
		NEW.close_dttm = NOW();
		RETURN NEW;
	ELSIF NEW.application_status_id = 2 THEN
		FOR column_val, new_value_val IN (SELECT pc.name, pad.new_value 
										  FROM profile_application_details pad
									      	INNER JOIN profile_columns pc on pad.profile_columns_id = pc.id
										  WHERE profile_applications_id = OLD.id)
  		LOOP
			query := format(
				'UPDATE profile set %I = %L, last_update_dttm = NOW()
				WHERE iin_bin = %L', column_val, new_value_val, OLD.iin_bin);
				
			INSERT INTO query_logs(sql_query) VALUES (query);
			EXECUTE query;
		END LOOP;
		NEW.close_dttm = NOW();
		RETURN NEW;
	ELSE
		RETURN OLD;
	END IF;
END;
$BODY$;

ALTER FUNCTION public.update_profile_on_change()
    OWNER TO postgres;