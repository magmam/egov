CREATE OR REPLACE FUNCTION public.get_profile_info(
	iin_bin_val character varying)
    RETURNS json
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
DECLARE
    response json;
BEGIN
	SELECT json_build_object(
		'fio', p.fio,
        'iin_bin', p.iin_bin,
        'birth_date', p.birth_date,
        'birth_place', COALESCE(p.birth_place, 'None'),
		'nationality', COALESCE(p.nationality, 'None'),
        'sex', COALESCE(g.name, 'None'),
        'email', COALESCE(p.email, 'None'),
        'phone_number', COALESCE(p.phone_number, 'None'),
		'education', COALESCE(p.education, 'None'),
        'current_address', COALESCE(p.current_address, 'None')
    ) INTO response
    FROM profile p
		LEFT JOIN gender g on CAST(p.sex as integer) = g.id
    WHERE iin_bin = iin_bin_val;

    RETURN response;
END;
$BODY$;

ALTER FUNCTION public.get_profile_info(character varying)
    OWNER TO postgres;