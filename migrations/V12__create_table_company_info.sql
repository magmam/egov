CREATE TABLE IF NOT EXISTS public.company_info
(
    bin character(12) COLLATE pg_catalog."default" NOT NULL,
    name character varying(255) COLLATE pg_catalog."default",
    registerdate timestamp without time zone,
    okedcode character varying(255) COLLATE pg_catalog."default",
    okedname character varying(255) COLLATE pg_catalog."default",
    secondokeds character varying(255) COLLATE pg_catalog."default",
    krpcode character varying(255) COLLATE pg_catalog."default",
    krpname character varying(255) COLLATE pg_catalog."default",
    krpbfcode character varying(255) COLLATE pg_catalog."default",
    krpbfname character varying(255) COLLATE pg_catalog."default",
    ksecode character varying(255) COLLATE pg_catalog."default",
    ksename character varying(255) COLLATE pg_catalog."default",
    kfscode character varying(255) COLLATE pg_catalog."default",
    kfsname character varying(255) COLLATE pg_catalog."default",
    katocode character varying(255) COLLATE pg_catalog."default",
    katoid integer,
    katoaddress character varying(255) COLLATE pg_catalog."default",
    fio character varying(255) COLLATE pg_catalog."default",
    ip boolean,
    valid_fr timestamp without time zone DEFAULT now(),
    valid_to timestamp without time zone DEFAULT (now() + '24:00:00'::interval),
    CONSTRAINT company_info_pkey PRIMARY KEY (bin)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.company_info
    OWNER to postgres;