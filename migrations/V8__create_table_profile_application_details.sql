CREATE TABLE IF NOT EXISTS public.profile_application_details
(
    profile_applications_id character(14) COLLATE pg_catalog."default" NOT NULL,
    profile_columns_id integer NOT NULL,
    old_value character varying(255) COLLATE pg_catalog."default",
    new_value character varying(255) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT profile_application_details_pkey PRIMARY KEY (profile_applications_id, profile_columns_id),
    CONSTRAINT profile_application_details_profile_columns_id_fkey FOREIGN KEY (profile_columns_id)
        REFERENCES public.profile_columns (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.profile_application_details
    OWNER to postgres;