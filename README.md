# Project Name - Simple EGOV

## Short Description

Simple EGOV is our project made for the Advanced Python course in Kazakh-British Technical University. Frontend part of the project is availible here https://gitlab.com/magmam/egovfront. 

## Detailed Description

Scheme of the application is availible here: https://excalidraw.com/#json=G5xjarvTKmuQBSJiCV_gM,SoVF9cLJte_N09ThxBAskQ

**For User**
1. Login Page
  - If the user already has an account:
    - Provide IIN/BIN and password to access the system.
  - If the user doesn't have an account:
    - Navigate to the Registration Page.
  - Upon logging in, the user is directed to the Home Page.

2. Registration Page
  - To create an account, users need to provide the following information:
    - IIN/BIN
    - Password
    - Full Name
    - Birthplace
    - Nationality
- Upon successful registration, the user is directed to the Login Page.

3. Home Page
  - Users can navigate to the Personal Account Page.
  - Users can navigate to the Applications Home Page.

4. Personal Account Page
  - Here, users can view all information about their account.

5. Applications Home Page
  - Users can view all their applications on this page, including dates and statuses.
  - Users can create two types of applications:
    - Account Information Change (Navigate to "Account Information Change Application Page").
    - Information on a Legal Entity (Navigate to "Information on a Legal Entity Application Page").

6. Account Information Change Application Page
  - Users can change information such as:
    - Phone Number
    - Email
    - Place of Study
  - Information such as IIN/BIN is unchangeable.

7. Information on a Legal Entity Application Page
  - Users can search for legal entities to obtain information about them.
  - If such information exists, users will be provided with a Telegram bot link to get a PDF file. Additionally, information will be sent to the user's email.

**For Manager**
1. Login Page
 - Managers need to enter their IIN/BIN along with a password to access the system.

2. Users Request Page
 - Upon logging in, the manager is directed to a page displaying all unanswered user requests.
 - Each request is listed with relevant details.

3. Request Answering Page
Managers can:
 - Open any request to view details.
 - Approve or reject the selected request.
 - After providing an answer, the manager is returned to the User Request Page.


### Responsible People

- **Kenen Rauan**
  - ID: 20B030349
  - Responsibilities: Mainly responsible for backend development. Has minor commitment for frontend development.

- **Isaev Zhasulan**
  - ID: 20B030318
  - Responsibilities: Mainly responsible for backend development. Has minor commitment for frontend development.

- **Raimbekov Daniyar**
  - ID: 20B030477
  - Responsibilities: Mainly responsible for database development. Has minor commitment for frontend and backend development.

- **Maxat Ualikhan**
  - ID: 20B030757
  - Responsibilities: Mainly responsible for database development. Has minor commitment for frontend development.

- **Kenesova Aidana**
  - ID: 20B030705
  - Responsibilities: Has done most of the work for frontend part of the project.

## Installation and Usage
**On backend:**
### Prerequisites to install:
  - Make
  - Docker

### Follow These Steps:

#### Windows:

1. Install virtual environment and activate it:

    ```bash
    py -m pip install --user virtualenv
    py -m venv env
    .\env\Scripts\activate
    ```

2. Install project dependencies:

    ```bash
    py -m pip install -r requirements.txt
    ```

3. Build and run the Docker containers:

    ```bash
    docker build
    docker compose up --build
    ```

4. Start the UVicorn server:

    ```bash
    uvicorn main:app --reload --port=8000 --host=0.0.0.0
    ```

   If you have Make and Chocolatey installed:

    ```bash
    make build
    make run
    make stop
    ```

5. Visit [http://localhost:3000/](http://localhost:3000/)

#### MacOS:

1. Install virtual environment and activate it:

    ```bash
    python3 -m pip install --user virtualenv
    python3 -m venv env
    source env/bin/activate
    ```

2. Install project dependencies:

    ```bash
    python3 -m pip install -r requirements.txt
    ```

3. Build and run the Docker containers:

    ```bash
    docker compose up --build  # or 'docker compose up' if image already exists
    ```

4. Visit [http://localhost:3000/](http://localhost:3000/)

**On frontend:**
### `npm start`

Runs the app in development mode. Open [http://localhost:3000](http://localhost:3000) to view it in your browser. The page will reload when you make changes. Lint errors, if any, will be displayed in the console.

### `npm test`

Launches the test runner in interactive watch mode. Refer to the testing section for more information.

### `npm run build`

Builds the app for production to the `build` folder. It bundles React in production mode, optimizes the build for performance, and includes minification with hashed filenames. Your app is ready for deployment. Refer to the deployment section for more information.

### `npm run eject`

**Note:** This is a one-way operation. Ejecting removes the single build dependency, allowing full control over configuration files and dependencies.

If you aren't satisfied with the build tool and configuration choices, you can eject at any time. This copies all configuration files and dependencies (webpack, Babel, ESLint, etc.) into your project, giving you complete control. All commands except eject still work but point to the copied scripts. Ejecting is optional, and the curated feature set is suitable for small to medium deployments. Use this feature when you're ready to customize the build process.

You don't have to ever use eject; it's entirely up to you and your project's needs.

## Contributing

We welcome contributions from the community! If you encounter any issues, have suggestions, or want to contribute to the project, please follow the guidelines below.

### Reporting Bugs/Issues

If you come across a bug or issue, please help us improve by reporting it. To report a bug:

1. Go to the Issues section.
2. Click on "New Issue."
3. Choose the appropriate template for reporting a bug.
4. Provide a clear and detailed description of the issue, including steps to reproduce it.
5. Attach any relevant screenshots or error messages.

### Contributing to the Project
- Send an email to [adv_python@gmail.com] to contact our project members to discuss contribution.

## Interesting features
1. This project contains migrations
2. You can register in telegram
3. Chat IDs are saved in the database
4. Procedures for the database are prescribed